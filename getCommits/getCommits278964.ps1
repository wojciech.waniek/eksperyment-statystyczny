$ErrorActionPreference = "Stop"

$allCommits = @()
$i = 1
$projectId = "278964"
while ($true) {
    Write-Output "page: $i"    
    $pagedCommits = (Invoke-WebRequest -Uri "https://gitlab.com/api/v4/projects/$projectId/repository/commits?with_stats=true&page=$i").Content | ConvertFrom-Json
    if ( $pagedCommits.Count -eq 0 ) {
        break;
    }
    $allCommits = $allCommits + $pagedCommits
    $i++;
}

$allCommits | Select-Object -Property id, created_at, author_email, 
@{Name = "stats"; Expression = { $_.stats | Select-Object additions, deletions, total } } |
Select-Object -Property * -ExcludeProperty stats -ExpandProperty stats |
Export-Csv -Path "commits_$projectId.csv"  -NoTypeInformation