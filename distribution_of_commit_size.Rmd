```{r}
commits = read.csv(file="commits_13083.csv",colClasses = c("character","character","character"), header = TRUE)
commits= transform(commits, created_at = as.POSIXct(created_at, "%Y-%m-%dT%H:%M:%S", tz="UTC"))
commits=transform(commits, total = as.numeric(total))
commits
```

```{r}

hist(log(commits$total),breaks = 20,freq = FALSE)

```



```{r}

plot(density(log(commits$total)),main="Density estimate of data")

```

```{r}

plot(ecdf(log(commits$total)), main="Empirical cumulative distribution function")

```





```{r}

x.norm<-rnorm(n=200,m=10,sd=2)
z.norm<-(x.norm-mean(x.norm))/sd(x.norm) ## standardized data
qqnorm(z.norm) ## drawing the QQplot
abline(0,1) ## drawing a 45-degree reference line

```




```{r}

x.wei<-rweibull(n=200,shape=2.1,scale=1.1) ## sampling from a Weibull distribution with parameters shape=2.1 and scale=1.1
x.teo<-rweibull(n=200,shape=2, scale=1) ## theorical quantiles from a Weibull population with known paramters shape=2 e scale=1
qqplot(x.teo,x.wei,main="QQ-plot distr. Weibull") ## QQ-plot
abline(0,1) ## a 45-degree reference line is plotted

```


















































